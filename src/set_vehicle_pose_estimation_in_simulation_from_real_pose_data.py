def set_vehicle_pose_estimation_in_simulation_from_real_pose_data(self, real_pose):
    
    ### preventing frequent update
    t_diff = time.time() - self._prev_time        
    if t_diff < self._update_rate:
        return
    self._prev_time = time.time()

    ### convert coordinates        
    _x, _y = self.transform_coordinates.transform(real_pose.latitude, real_pose.longitude)
    # get estimated ego location
    ego_location = Location(x=_x,y=-_y,z=max(real_pose.height, self.actor.get_location().z))

    _pitch = real_pose.pitch
    _roll = real_pose.roll
    _yaw = real_pose.azimuth -90.0

    ego_rotation = Rotation(_roll, _pitch,_yaw,)
    ego_velocity = Vector3D(real_pose.north_velocity, -real_pose.east_velocity, real_pose.up_velocity)

    ### get more exact estimated location and rotation of ego
    front_ego_location = Location(0, 0, 0)
    front_ego_location.x = ego_location.x + math.cos(math.radians(ego_rotation.yaw)) * 2.7
    front_ego_location.y = ego_location.y + math.sin(math.radians(ego_rotation.yaw)) * 2.7
    front_ego_location.z = ego_location.z

    # fix ego height
    ego_ground_point = self.world.ground_projection(ego_location)
    if ego_ground_point != None:
        ego_location.z = ego_ground_point.location.z

    front_ego_ground_point = self.world.ground_projection(front_ego_location)
    if front_ego_ground_point != None:
        front_ego_location.z = front_ego_ground_point.location.z

    # fix ego rotation 
    z_diff = front_ego_location.z - ego_location.location.z
    calculated_pitch = math.atan(z_diff/self.actor.get_wheelbase())
    ego_rotation.pitch = calculated_pitch

    ### set estimated ego location and rotation in unreal engine simulation
    vehicle_transform = Transform(ego_location, ego_rotation)
    self.actor.set_transform(vehicle_transform)
    self.actor.set_velocity(ego_velocity)