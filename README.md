# **Virtual Vehicle Pose Estimation for VIL Simulation**

Welcome to our project, where the area of virtual and real-world driving converge! This repository houses an innovative approach to Vehicle-in-the-Loop (VIL) simulation, focusing on accurately translating real vehicle pose data into a virtual environment. Our solution aims to enhance simulation realism and fidelity, providing a crucial tool for automotive research, development, and testing.

## **Overview**

In the dynamic world of automotive technology, the ability to seamlessly blend real-world data into virtual simulations is invaluable. This project addresses this need by implementing a method to estimate a virtual vehicle's pose based on real-world vehicle pose data. Our core algorithm efficiently translates real-world coordinates, orientation, and movement into the simulated environment, enabling a synchronized and dynamic driving experience.

## **Key Features**
- **Real-to-Virtual Pose Translation**: This feature takes real-world vehicle pose data (latitude, longitude, pitch, roll, azimuth) and converts it into corresponding virtual world coordinates.
- **Efficient Update Mechanism**: Our system ensures that the simulation is updated at optimal intervals to avoid unnecessary computational overhead.
- **Dynamic Elevation and Orientation Adjustments**: The algorithm keeps the virtual vehicle's elevation and orientation in line with real-world data, accommodating terrain variations in the virtual environment.

## **Implementation**

The core of our approach lies in the **`set_vehicle_pose_estimation_in_simulation_from_real_pose_data`** function. This function takes real-world pose data as input and performs several critical operations to estimate the virtual vehicle's pose in the simulation. Key steps include:

- **Update Rate Management**: Implements throttling of updates to ensure an efficient update rate without compromising data freshness.
- **Coordinate Transformation**: Transforms real-world geographical data to align accurately with the virtual environment's coordinate system.
- **Elevation and Orientation Tuning**: Adjusts the virtual vehicle's elevation and orientation to mirror real-world terrain and direction, enhancing the realism of the simulation.
- **Adaptive Rotation Adjustment**: Makes real-time adjustments to the virtual vehicle's rotation based on the calculated elevation differences, maintaining fidelity to the real-world vehicle's orientation.
- **Simulation Integration**: Applies the transformed pose and velocity within the Unreal Engine simulation environment, ensuring that the virtual experience closely replicates the real-world scenario.

## **Usage**

## **Installation**